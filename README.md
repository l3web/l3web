# README #

## Architecture du projet ##

**analyse** : Diagrammes, documents, etc.

**framework** : Racine du projet web

**config** : Fichiers de configuration 

**parts** : Fichiers "durs" du site

**vendor** : Elements externes

**assets** : Fichiers CSS et JS

**res** : Images, sons, etc.

## Concernant les noms de fichiers et de classes :

* Contrôleurs : 
    - nom de la classe avec une majuscule en début de nom et une majuscule pour chaque nouveau mot exemple (class AccueilPrincipal)
    - nom du fichier avec "c_" devant et **tout en minuscule** (c_accueilprincipal.php)
    
* Modèle : 
    - nom de la classe avec un "_m" en fin, une majuscule en début de nom et une majuscule pour chaque nouveau mot exemple (class AccueilPrincipal_m)
    - nom du fichier avec "m_" devant et **tout en minuscule** (m_accueilprincipal.php)
    
* Vues : 
    - nom du fichier avec "v_" devant et **tout en minuscule** (v_accueilprincipal.php)

## Concernant le code ##

* Indenter correctement et respecter [les conventions de nommage] (http://jcrozier.developpez.com/tutoriels/web/php/conventions-nommage/)

* Commenter clairement chacune de ses fonctions

## Concernant git ##

* Toujours faire un update avant de commit

* Ne jamais commit une version avec des bugs, ou alors le faire sur une autre branche que master

* Toujours mettre un message bref pour les commits


``Pensez à paramètrer le fichier config/config.php !``
-------------------------------------------------

**Pour le BASE_URL :** Chemin http jusqu'à framework/  (Exemple : http://localhost/web/CollaMaps/l3web/framework/)
**Ne pas oublier '/' après framework**