
    function addElement(forme) {
        var xmlns = 'http://www.w3.org/2000/svg';
        var xhtml = 'http://www.w3.org/1999/xhtml';
        var newG = document.createElementNS(xmlns,"g");
        var svg_edition = document.getElementById('zone_edit');
        svg_edition.appendChild(newG);
        newG.setAttributeNS(null,'transform','matrix(1 0 0 1 0 0)');
        newG.setAttributeNS(null,'onclick','del(evt)')
        newG.setAttributeNS(null,'class','draggable');
        newG.setAttributeNS(null,'width','99.9%');
        newG.setAttributeNS(null,'height','99.5%');
        var color = document.getElementById('innercolor').value;
        var border_color = document.getElementById('bordercolor').value;
        var rounded = document.getElementById('arrondi');

        var roundedbool = false;
        if(rounded.checked)
            roundedbool = true;

        var form_size = document.getElementById('form_size').value;
        var border_size = document.getElementById('strokewidth').value;

        if(form_size < 1)
            form_size = 1;

        if(border_size < 1)
            border_size = 1;

        form_size = parseInt(form_size);
        border_size = parseInt(border_size);



        switch (forme){
            case 'carre' :

                var size = 60+10*form_size;
                var posx = 330+form_size*1.5;
                var posy = form_size*5+50;
                var text_size = form_size+4*1.2;
                var max_text_size = text_size+form_size;

                if(roundedbool)
                {
                    newG.innerHTML = "<rect x='330' y='30' width='" + size + "' height='" + size + "' rx='20' ry='20' style='fill:" + color + ";stroke:" + border_color + ";stroke-width:" + border_size + "'/>";
                }
                else
                {
                    newG.innerHTML = "<rect x='330' y='30' width='" + size + "' height='" + size + "' style='fill:" + color + ";stroke:" + border_color + ";stroke-width:" + border_size + "'/>";
                }

                var text_zone = document.createElementNS(xmlns,"foreignObject");
                newG.appendChild(text_zone);
                newG.setAttributeNS(null,'onmousedown','selectMe(evt)');
                text_zone.setAttributeNS(null,'x','330');
                text_zone.setAttributeNS(null,'y','20');
                text_zone.setAttributeNS(null,'width',size);
                text_zone.setAttributeNS(null,'height',size);
                text_zone.setAttributeNS(null,'alignment-baseline','middle');

                var div_zone = document.createElementNS(xhtml,"div");
                text_zone.appendChild(div_zone);
                div_zone.setAttribute('xhtml','http://www.w3.org/1999/xhtml');


                div_zone.innerHTML = "<p width='" + size + "' height='" + size + "' class='toolbox-p' contenteditable='true'>First steps...</p>";

                break;

            case 'rectangle' :

                var sizex = 120+20*form_size;
                var sizey = 60+10*form_size;
                var posx = 335;
                var posy = form_size*5+50;
                var text_size = form_size*2.85+11;
                var max_text_size = text_size+form_size;

                if(roundedbool)
                {
                    newG.innerHTML = "<rect x='330' y='30' width='" + sizex + "' height='" + sizey + "' rx='20' ry='20' style='fill:" + color + ";stroke:" + border_color + ";stroke-width:" + border_size + "'/>";
                }
                else
                {
                    newG.innerHTML = "<rect x='330' y='30' width='" + sizex + "' height='" + sizey + "' style='fill:" + color + ";stroke:" + border_color + ";stroke-width:" + border_size + "'/>";
                }

                var text_zone = document.createElementNS(xmlns,"foreignObject");
                newG.appendChild(text_zone);
                newG.setAttributeNS(null,'onmousedown','selectMe(evt)');
                text_zone.setAttributeNS(null,'x','360');
                text_zone.setAttributeNS(null,'y','20');
                text_zone.setAttributeNS(null,'width',sizex);
                text_zone.setAttributeNS(null,'height',sizey);
                text_zone.setAttributeNS(null,'alignment-baseline','middle');

                var div_zone = document.createElementNS(xhtml,"div");
                text_zone.appendChild(div_zone);
                div_zone.setAttribute('xhtml','http://www.w3.org/1999/xhtml');

                div_zone.innerHTML = "<p width='" + size + "' height='" + size + "' class='toolbox-p' contenteditable='true'>First steps...</p>";

                break;

            case 'ellipse' :

                var sizex = 120+7*form_size;
                var sizey = 60+4*form_size;
                var posx = 260-form_size*4;
                var posy = 88;
                var text_size = form_size*1.2+29;
                var max_text_size = text_size+form_size;

                newG.innerHTML = "<ellipse cx='380' cy='100' rx='" + sizex + "' ry='" + sizey + "' rx='20' ry='20' style='fill:" + color + ";stroke:" + border_color + ";stroke-width:" + border_size + "'/>";


                var text_zone = document.createElementNS(xmlns,"foreignObject");
                newG.appendChild(text_zone);
                newG.setAttributeNS(null,'onmousedown','selectMe(evt)');
                text_zone.setAttributeNS(null,'x','340');
                text_zone.setAttributeNS(null,'y','30');
                text_zone.setAttributeNS(null,'width',sizex);
                text_zone.setAttributeNS(null,'height',sizey);
                text_zone.setAttributeNS(null,'alignment-baseline','middle');

                var div_zone = document.createElementNS(xhtml,"div");
                text_zone.appendChild(div_zone);
                div_zone.setAttribute('xhtml','http://www.w3.org/1999/xhtml');

                div_zone.innerHTML = "<p width='" + size + "' height='" + size + "' class='toolbox-p' contenteditable='true'>First steps...</p>";

                break;

            case 'cercle' :

                var size = 60+4*form_size;
                var posx = 330-form_size*3.5;
                var posy = 88;
                var text_size = form_size*1.2+9;
                var max_text_size = text_size+form_size;

                newG.innerHTML = "<ellipse cx='380' cy='100' rx='" + size + "' ry='" + size + "' rx='20' ry='20' style='fill:" + color + ";stroke:" + border_color + ";stroke-width:" + border_size + "'/>";


                var text_zone = document.createElementNS(xmlns,"foreignObject");
                newG.appendChild(text_zone);
                newG.setAttributeNS(null,'onmousedown','selectMe(evt)');
                text_zone.setAttributeNS(null,'x','340');
                text_zone.setAttributeNS(null,'y','50');
                text_zone.setAttributeNS(null,'width',size);
                text_zone.setAttributeNS(null,'height',size);
                text_zone.setAttributeNS(null,'alignment-baseline','middle');

                var div_zone = document.createElementNS(xhtml,"div");
                text_zone.appendChild(div_zone);
                div_zone.setAttribute('xhtml','http://www.w3.org/1999/xhtml');

                div_zone.innerHTML = "<p x='340' y='20' width='" + size + "' height='" + size + "' onkeypress='verifierCaracteres(event); return false;' class='toolbox-p' contenteditable='true'>First steps...</p>";

                break;
        }
    }

    function verifierCaracteres(event) {

        var keyCode = event.which ? event.which : event.keyCode;
        var touche = String.fromCharCode(keyCode);

        var champ = evt.target;

        var caracteres = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789éèà ()[]{}&#.,?:!';

        if(caracteres.indexOf(touche) >= 0) {
            champ.value += touche;
        }

    }


