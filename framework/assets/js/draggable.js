var selectedElement = 0;
var currentX = 0;
var currentY = 0;
var currentMatrix = 0;
var type = "";


    function selectElement(evt,type) {
        selectedElement = evt.target.parentNode;
        currentX = evt.clientX;
        currentY = evt.clientY;
        currentMatrix = selectedElement.getAttributeNS(null, "transform").slice(7,-1).split(' ');

        for(var i=0; i<currentMatrix.length; i++) {
            currentMatrix[i] = parseFloat(currentMatrix[i]);
        }

        selectedElement.setAttributeNS(null, "onmousemove", "moveElement(evt)");
        selectedElement.setAttributeNS(null, "onmouseup", "deselectElement(evt)");
        selectedElement.setAttributeNS(null, "onmouseout", "deselectElement(evt)");
    }


    function moveElement(evt) {
        var dx = evt.clientX - currentX;
        var dy = evt.clientY - currentY;

        currentMatrix[4] += dx;
        currentMatrix[5] += dy;

        selectedElement.setAttributeNS(null, "transform", "matrix(" + currentMatrix.join(' ') + ")");
        currentX = evt.clientX;
        currentY = evt.clientY;
    }

    function deselectElement(evt) {
        if (selectedElement != 0) {
            type = evt.target.tagName;
            if(type == "cercle" || type == "ellipse")
            {
                currentX = parseInt(evt.target.getAttributeNS(null,"cx"));
                currentY = parseInt(evt.target.getAttributeNS(null,"cy"));
                evt.target.setAttributeNS(null,"cx",currentX+currentMatrix[4]);
                evt.target.setAttributeNS(null,"cy",currentY+currentMatrix[5]);
                selectedElement.setAttributeNS(null, "transform", "matrix(1 0 0 1 0 0)");
                selectedElement.removeAttributeNS(null, "onmousemove");
                selectedElement.removeAttributeNS(null, "onmouseout");
                selectedElement.removeAttributeNS(null, "onmouseup");
                selectedElement = 0;
            }
            else
            {
                currentX = parseInt(evt.target.getAttributeNS(null,"x"));
                currentY = parseInt(evt.target.getAttributeNS(null,"y"));
                evt.target.setAttributeNS(null,"x",currentX+currentMatrix[4]);
                evt.target.setAttributeNS(null,"y",currentY+currentMatrix[5]);
                selectedElement.setAttributeNS(null, "transform", "matrix(1 0 0 1 0 0)");
                selectedElement.removeAttributeNS(null, "onmousemove");
                selectedElement.removeAttributeNS(null, "onmouseout");
                selectedElement.removeAttributeNS(null, "onmouseup");
                selectedElement = 0;
            }
            var fo = evt.target.parentNode.children[1];
            currentX = parseInt(fo.getAttributeNS(null,"x"));
            currentY = parseInt(fo.getAttributeNS(null,"y"));
            fo.setAttributeNS(null,"x",currentX+currentMatrix[4]);
            fo.setAttributeNS(null,"y",currentY+currentMatrix[5]);
        }
    }


var selectionOccurence = 0;
var xmlns = "http://www.w3.org/2000/svg";
var X1 = 0;
var Y1 = 0;
var X2 = 0;
var Y2 = 0;

    function selectMe(evt)
    {
        if(evt.ctrlKey)
        {

            switch(selectionOccurence)
            {
                case 0:
                        selectedElement = evt.target.parentNode;
                        var bbox = selectedElement.getBBox();
                        var ctm = selectedElement.getCTM()
                        var cx = bbox.x + bbox.width/2;
                        var cy = bbox.y + bbox.height/2;
                        var pt = selectedElement.parentNode.createSVGPoint();
                        pt.x = cx;
                        pt.y = cy;
                        pt = pt.matrixTransform(ctm);
                        X1 = cx;
                        Y1 = cy;
                        selectionOccurence++;
                        break;
                case 1:
                        selectedElement = evt.target.parentNode;
                        var bbox = selectedElement.getBBox();
                        var ctm = selectedElement.getCTM()
                        var cx = bbox.x + bbox.width/2;
                        var cy = bbox.y + bbox.height/2;
                        var pt = selectedElement.parentNode.createSVGPoint();
                        pt.x = cx;
                        pt.y = cy;
                        pt = pt.matrixTransform(ctm);
                        X2 = cx;
                        Y2 = cy;
                        var stroke = document.createElementNS(xmlns,'line');
                        stroke.setAttributeNS(null,"class","line");
                        stroke.setAttributeNS(null,"x1",X1);
                        stroke.setAttributeNS(null,"y1",Y1);
                        stroke.setAttributeNS(null,"x2",X2);
                        stroke.setAttributeNS(null,"y2",Y2);
                        stroke.setAttributeNS(null,"style","stroke:black;stroke-width:2");
                        stroke.setAttributeNS(null,"onclick","del(evt)")
                        stroke = selectedElement.parentNode.insertBefore(stroke,selectedElement.parentNode.firstChild);
                        selectionOccurence--;
                        break;
                 default:
                        break;
             }
        }
        else
        {
            selectElement(evt);
        }


     }

    function del(evt)
    {
        if(evt.altKey)
        {
            selectedElement = evt.target.parentNode;
            selectedElement.parentNode.removeChild(selectedElement);
        }
        else {

        }
    }





