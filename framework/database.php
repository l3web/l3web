<?php
class Database
{
    static protected $_instance = null;
    protected $_db;

    static public function getInstance()
    {
        if (is_null(self::$_instance))
            self::$_instance = new Database();
        return self::$_instance;
    }

    protected function __construct()
    {
        try {
            $dns = 'mysql:host=' . host . ';dbname=' . dbname;
            $utilisateur = user;
            $motDePasse = password;
            //PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",     // pour l'encodage
            $options = array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION           // pour afficher les erreurs
            );
            $connection = new PDO($dns, $utilisateur, $motDePasse, $options);
            $this->_db = $connection;
        } catch (Exception $e) {
            echo "Connection à MySQL impossible : ", $e->getMessage();
            die();
        }
    }

    public function __call($method, array $arg) {
    // Si on appelle une méthode qui n'existe pas, on
    // délègue cet appel à l'objet PDO $this->_db
        return call_user_func_array(array($this->_db, $method), $arg);
    }
}