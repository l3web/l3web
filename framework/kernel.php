<?php
define("SITE_PATH",realpath(dirname(__FILE__)).'/');
define("PARTS", SITE_PATH."parts/");
define("CONTROLLERS", PARTS."controllers/");
define("VIEWS", PARTS."views/");
define("MODELS", PARTS."models/");
define("ASSETS", SITE_PATH."assets/");
define("CSS", ASSETS."css/");
define("JS", ASSETS."js/");
define("RES", SITE_PATH."res/");

$tab_url1=explode('/', $_SERVER['REQUEST_URI']);
$tab_url2=explode('/', $_SERVER['SCRIPT_NAME']);

//On récupère ce qui se trouve après index.php/
$tab3=array_diff($tab_url1, $tab_url2);

//Le premier paramètre récupéré est le contrôlleur
//S'il est vide, on récupère le contrôlleur par défaut, sinon on prend celui passé en paramètre
if($c=array_shift($tab3)) $_controller=$c; else $_controller=default_controller;

//Le deuxième paramètre est la fonction
//Si elle est vide, on appelle la fonction index, sinon celle passée en paramètre
if($m=array_shift($tab3)) $_method=$m; else $_method="index";

//S'il y en a, on récupère les paramètres
if(isset($tab3[0])) $_args=$tab3; else 	$_args=array();

//On vérifie si le fichier du contrôlleur existe
$controllerFile=SITE_PATH."parts/controllers/c_".strtolower($_controller).".php";
if(is_readable($controllerFile))
{
    require($controllerFile);

    //On instancie le contrôlleur et on vérifie si la méthode existe
    $controller = new $_controller;
    if(is_callable(array($controller,$_method)))
        $method=$_method;
    else
        $method="index";

    //On appelle la fonction avec les paramèters s'ils ont été spécifiés
    if(!empty($_args))
        call_user_func_array(array($controller,$method),$_args);
    else
        call_user_func(array($controller,$method));
}
//On affiche un message d'erreur si le fichier du contrôleur n'existe pas
else {
    require(SITE_PATH . "parts/controllers/c_principal.php");
    $controller = new Principal();
    call_user_func(array($controller, "erreur"));
}
?>
