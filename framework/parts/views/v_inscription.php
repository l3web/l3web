<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?=BASE_URL?>assets/css/formulaire.css"/>
    <script type="text/javascript" src="<?=BASE_URL?>vendor/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?=BASE_URL?>assets/js/scriptJquery.js"></script>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/header.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/menu_vertical.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/footer.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/pages.css" media="screen"/>
    <title>CollaMaps</title>
</head>
<body>

<?php
include(VIEWS."Template/nav.php");
include(VIEWS."Template/menu.php");
?>

<div align="center" id="bloc">
    <div id="cadre">
        <div id="titre">Inscription</div>
        <div id="formulaire">
            <form method="post" action="<?=BASE_URL?>index.php/Users/inscription"><br/>
                <div id="inputs">
                    <label>Pseudo</label><br/>
                    <input id="pseudo" type="text" name="pseudo" value=""required onkeyup="checkPasswd(); return false;"/><br/><br/>
                    <label>Prénom</label><br/>
                    <input id="prenom" type="text" name="prenom" value=""required onkeyup="checkPasswd(); return false;"/><br/><br/>
                    <label>Nom</label><br/>
                    <input id="nom" type="text" name="nom" value=""required onkeyup="checkPasswd(); return false;"/><br/><br/>
                    <label>Adresse mail</label><br/>
                    <input id="email" type="email" name="email" value=""required onkeyup="checkPasswd(); return false;"/><br/><br/>
                    <label>Mot de passe</label><br/>
                    <input id="pass1" type="password" name="mdp"value="" required onkeyup="checkPasswd();return false;"/><br/><br/>
                    <label>Valider votre mot de passe</label><br/>
                    <input id="pass2" type="password" name="mdp" value="" required onkeyup="checkPasswd();return false;"/><br/><br/>
                </div>
                <div id="Valider"><input id="ok" type="submit" class="submit" value="Ok" disabled></div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    checkPasswd();

    function checkPasswd()
    {
        var pass1 = document.getElementById('pass1');
        var pass2 = document.getElementById('pass2');
        var ok = document.getElementById("ok");

        if (pass1.value == "") {
            ok.disabled = true;
        } else if(pass1.value == pass2.value){
            pass2.style.backgroundColor = "#3d9c2a";
            ok.disabled = !checkInputs();
        }else{
            pass2.style.backgroundColor = "#FF4650";
            ok.disabled = true;

        }
    }

    //Renvoie true si tous les champs sont remplis, false sinon
    function checkInputs() {
        var pseudo = document.getElementById('pseudo');
        var prenom = document.getElementById('prenom');
        var nom = document.getElementById('nom');
        var email = document.getElementById('email');

        if (pseudo.value != "" && prenom.value != "" && nom.value != "" && email.value != "") {
            return true;
        } else {
            return false;
        }
    }
</script>
</body>
</html>
