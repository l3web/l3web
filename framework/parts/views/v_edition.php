<!DOCTYPE html>
<html>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
    <title>CollaMaps</title>
    <meta charset="UTF-8" />

    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/edition.css" />
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/header.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/menu_vertical.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/footer.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/formulaire.css" media="screen"/>

    <link rel="stylesheet" href="<?=BASE_URL?>assets/css/edition.css"/><!--à changer, j'ai pas encore fait le controleur-->
    <script type="text/javascript" src="<?=BASE_URL?>vendor/jquery-1.11.2.min.js"></script>
    <script src="<?=BASE_URL?>assets/js/draggable.js" type="text/javascript"></script><!--à changer, j'ai pas encore fait le controleur-->
    <script src="<?=BASE_URL?>assets/js/createBox.js" type="text/javascript"></script><!--à changer, j'ai pas encore fait le controleur-->
    <script src="<?=BASE_URL?>assets/js/saveCard.js" type="text/javascript"></script><!--à changer, j'ai pas encore fait le controleur-->
    <script src="<?=BASE_URL?>assets/js/d3.js"></script>
</head>
<head>
    <title>CollaMaps</title>
</head>

<?php
    header("X-XSS-Protection: 0");
    include(VIEWS."Template/nav.php");
    include(VIEWS."Template/menu.php");
?>

<body onunload="alert()">
    <form id="edition" action="<?=BASE_URL?>index.php/Carte/saveInDb/<?php echo $nom_carte;?>" method="post">

        <?php if (strcmp($_SESSION['role']["$nom_carte"], "Consultant") !== 0) { ?>
        <div id="toolbox">
            <svg id="svgtoolsquare" width=100% height=100%>
                <rect id="toolsquare" x="5" y="5" width="200" height="550" style="opacity:0.5;"/>
                <foreignObject width="200" height="550">
                    <div id="tools" width=100% height=100%>
                        <div class="tool">
                            <input type="text" value="<?php echo $nom_carte;?>" name="nom_carte" style="width:100%" disabled/>
                            <input type="hidden" name="smthg" id="smthg" value="">
                        </div>
                        <div class="tool">
                            <p class="toolbox-p">Couleur de remplissage :</p>
                            <input id="innercolor" class="color-input" type="color" name="innercolor" value="#ffff00"/>
                        </div>
                        <div class="tool">
                            <p class="toolbox-p">Couleur de contour :</p>
                            <input id="bordercolor" class="color-input" type="color" name="bordercolor" value="#000000" />
                        </div>
                        <div class="tool">
                            <p class="toolbox-p">Epaisseur de contour :</p>
                            <input id="strokewidth" class="color-input" type="text" name="strokewidth" size="2" maxlength="2"/>
                        </div>
                        <div class="tool">
                            <p class="toolbox-p">Taille :</p>
                            <input id="form_size" class="color-input" type="text" name="form_size" size="2" maxlength="1"/>
                        </div>
                        <div class="tool">
                            <p class="toolbox-p">Coins arrondis :</p>
                            <input id="arrondi" class="color-input" type="checkbox" name="arrondi"/>
                        </div>
                        <div class="tool">
                            <input type="button" onclick="addElement('cercle')" name="cercle" value="Cercle" style="width:45%;"/>
                            <input type="button" class="color-input" onclick="addElement('carre')" name="carre" value="Carré" style="width:45%;"/>
                            <input type="button" onclick="addElement('ellipse')" name="ellipse" value="Ellipse" style="width:45%;"/>
                            <input type="button" class="color-input" onclick="addElement('rectangle')" name="rectangle" value="Rectangle" style="width:45%;"/>
                        </div>
                        <div class="tool">
                            <p class="toolbox-p">Maintenez Ctrl en cliquant sur deux formes pour les lier.</p>
                        </div>
                        <div class="tool">
                            <p class="toolbox-p">Maintenez Alt en cliquant sur une forme pour la supprimer.</p>
                        </div>
                        <div class="tool">
                            <input name="saveinDB" onclick="savecard()" type="button" value="Enregistrer" style="width:100%;font-size:12px;"/>
                        </div>
                        <div class="tool">
                            <input name="quitter" onclick="quit()" type="button" value="Quitter" style="width:100%;font-size:12px;"/>
                        </div>
                    </div>
                </foreignObject>
            </svg>
        </div>
        <?php } ?>

        <div id="truc">
            <?php
                if(isset($string_carte))
                    echo $string_carte;
                else
                    echo "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' id='zone_edit' width='100%' height='100%'>            
                            <rect x='0.5' y='0.5' width='99.9%' height='99.5%' fill='none' stroke='black'/>
                        </svg>";
            ?>
        </div>

    </form>
</body>
</html>