<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>CollaMaps</title>
	<link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/header.css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/menu_vertical.css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/footer.css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/formulaire.css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/pages.css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/gestionUsers.css" media="screen"/>

</head>

<body>

<?php
include(VIEWS."Template/nav.php");
include(VIEWS."Template/menu.php");
?>

<div align="center" id="blocU">
	<div id="cadreU">
	    <table class="table table-bordered">
            <caption id="titreU">Gestion des utilisateurs</caption>
			<thead>
				<tr>
				<th>E-mail</th>
				<th>Speudo</th>
				<th>Role</th>
				<th>Ban</th>
					<th>Historique</th>
				</tr>
			</thead>
			<tbody>
			<?php

			foreach($tab as $data)
			{
				$mail=$data->email;
				$droit=$data->id_droit;

					echo"<tr><td><input type='hidden' value='email[]' name='email'>".$data->email."</td>";
					echo"<td name='email'>".$data->pseudo."</td>";
							echo"<td>".$data->id_droit."</td>";
				if($droit=='Client'){
					$test = sizeof($m_user->recupBannis($mail));
					if ($test == 0) {
						//$i=1;
						//echo"<td><button type='button' class='banni' id='$mail'>Bannir</button></td>";
						echo "<th><span><a href='' class='banni' id='$mail'>Bannir</a></span></th>";
					} else {
						//$i=0;
						//echo"<td><button type='button' class='debanni' id='$mail'>Debannir</button></td>";
						echo "<th><span><a href='' class='debanni' id='$mail'>Debannir</a></span></th>";
					}
				}else echo"<td></td>";
				echo"<td><span ><input type='submit' id='$mail' name='submit' value='Historique'></input></span></td>";
					echo"</tr>";


			}

			?>
			</tbody>
		</table>
	</div>
	<h2><a href="<?= BASE_URL?>index.php/Users/listBan">Liste des personnes bannis</a></h2>
	<div id="histo">

	</div>
</div>
		<script type="text/javascript" src="<?=BASE_URL?>assets/js/jquery.js"></script>
		<script type="text/javascript">

					$(document).ready(function(){
						//quand il click sur Bannir
						$('.banni').click(function(){
							var mail= $(this).attr('id');
							var val= $(this).attr('class');
							var raison = prompt('donner les raisons !');
							//alert('email que vous avez clickez'+mail);
							$.ajax({
								url:'<?=BASE_URL?>index.php/Users/banUsers',
								type:'post',
								async: false,
								data:{
									'mail':mail,
									'val':val,
									'raison':raison
								},
								dataType: 'html',
								success: function(){
									//alert("slt");
								},
								error: function(){
									alert("slererret");
								}
							})

						})
						//quand il click sur Debannir
						$('.debanni').click(function(){
							var mail= $(this).attr('id');
							var val= $(this).attr('class');
							//alert('email que vous avez clickez'+mail);
							$.ajax({
								url:'<?=BASE_URL?>index.php/Users/banUsers',
								type:'post',
								async: false,
								data:{
									'mail':mail,
									'val':val

								},
								success: function(){

									//alert("slt2");
								},
								error: function(){
									alert("slteerr");
								}
							})

						})
						//script ajax pour l'historique
						//quand il click sur le button submit historique
						$('input[type="submit"]').click(function(){
							var mail= $(this).attr('id');
							//alert('email que vous avez clickez'+mail);
							$.ajax({
								url:'<?=BASE_URL?>index.php/Users/histo_ban',
								type:'post',
								async: false,
								data:{
									'mail':mail


								},
								success: function(data){
									//location.reload();
									if(data){
										//alert('bien');
										$('#histo').html(data);
									}else{
										$('#histo').fadeIn().text('pas trouve!');
										//alert('pas vu!');
									}

								}
							})

						})
					})
					//$.post('../ajax.ajax.php',{email_add:email},{})

		</script>


</body>
</html>