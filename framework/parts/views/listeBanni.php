<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>CollaMaps</title>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/header.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/menu_vertical.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/footer.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/formulaire.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/pages.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/gestionUsers.css" media="screen"/>

</head>

<body>

<div align="center" id="blocU">
    <div id="cadreU">
        <div id="titreU">Liste des personnes bannis</div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>E-mail</th>
                <th>Date Ban</th>
                <th>Raison</th>
            </tr>
            </thead>
            <tbody>
            <?php

            foreach($tab as $data)
            {
                $mail=$data->email_banni;
                $date=$data->date_debut;
                $raison=$data->raison;

                echo"<tr><td><input type='hidden'  name='email'>".$mail."</td>";
                echo"<td name='email'>".$date."</td>";
                echo"<td>".$raison."</td>";
                echo"</tr>";


            }

            ?>
            </tbody>
        </table>

    </div>
    <h2><a href="<?= BASE_URL?>index.php/Users/gestionUsers">Gestion des utilisateurs</a></h2>
</div>
</body>
</html>