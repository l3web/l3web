<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>CollaMaps</title>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/header.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/menu_vertical.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/footer.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/formulaire.css" media="screen"/>

</head>

<body>

<?php
include(VIEWS."Template/nav.php");
include(VIEWS."Template/menu.php");

echo "<h3>Le nom du contrôleur est invalide.</h3>";
?>
</body>
</html>