<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>CollaMaps</title>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/header.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/menu_vertical.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/footer.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/formulaire.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/pages.css" media="screen"/>
   
</head>

<body>

<?php
include(VIEWS."Template/nav.php");
include(VIEWS."Template/menu.php");
?>

<div align="center" id="bloc">
    <div id="cadre">
        <div id="titre">Recapitulatif</div>
        <div id="formulaire">
            <form method="POST" action="<?=BASE_URL?>index.php/users/modifierProfil">
                <label>Nom</label><br/>
                <input type="text" id="nom" name="nom" value="<?php echo $_SESSION['nom'] ?>" /><br/><br/>

                <label>Prenom</label><br/>
                <input type="text" id="prenom" name="prenom" value="<?php echo $_SESSION['prenom'] ?>" /><br/><br/>

                <label>Pseudo</label><br/>
                <input type="text" id="pseudo" name="pseudo"  value="<?php echo $_SESSION['pseudo'] ?>" /><br/><br/>

                <label>Email</label><br/>
                <input type="text" id="email" name="email" value="<?php echo $_SESSION['email'] ?>" disabled/><br/><br/>

                <label>Rôle</label><br/>
                <input type="text" id="id" name="id" value="<?php echo $_SESSION['id_droit'] ?>" disabled/><br/><br/>

                <label>Date d'inscription</label><br/>
                <input type="text" id="date" name="date" value="<?php echo $_SESSION['date_inscription'] ?>" disabled/><br/><br/>

                <label>Modifier votre mdp</label><br/>
                <input type="password" id="password" name="pass" value="<?php echo $_SESSION['mdp'] ?>" /><br/><br/>

                <label>Confirmer votre mdp</label><br/>
                <input type="password" id="npassword" name="npass"/><br/><br/><br/>

                <input type="Submit" value="Valider" id="valider"  />
                <a href=""><input type="Button" value="Annuler" id="annuler" /></a><br/>
            </form>
        </div>
    </div>
</div>
		
</body>
</html>
