<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>CollaMaps</title>

    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/header.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/menu_vertical.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/footer.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/formulaire.css"/>
</head>
<body>

<?php
include(VIEWS."Template/nav.php");
include(VIEWS."Template/menu.php");
?>

<div id="Page">

    <div align="center" id="bloc">
        <div id="cadre">
            <div id="titre">Mot de passe oublié</div>
            <div id ="formulaire">
                <form action="<?= BASE_URL?>index.php/Users/mailMDP" method="post">
                    <div id="inputs">
                        <br/>
                        <label>Adresse mail</label><br/>
                        <input type="email" name="email" class="box" required /><br /><br />
                    </div>
                    <input type="submit" name='submit' value="Valider" class='submit'/><br />
                    <?php
                    if(isset($errMsg)){
                        echo '<div id="error">'.$errMsg.'</div>';
                    }
                    ?>

                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
