<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?=BASE_URL?>assets/css/formulaire.css"/>
    <title>CollaMaps</title>
    <script type="text/javascript" src="<?=BASE_URL?>vendor/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?=BASE_URL?>assets/js/scriptJquery.js"></script>

     <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/header.css" media="screen"/>
     <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/menu_vertical.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/footer.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/pages.css" media="screen"/>
</head>
<body>

<?php
include(VIEWS."Template/nav.php");
include(VIEWS."Template/menu.php");
?>

    <div id="Page">

        <div align="center" id="bloc">
            <div id="cadre">
                <div id="titre">Login</div>
                <div id ="formulaire">
                    <form action="<?= BASE_URL?>index.php/Users/sessionConnexion" method="post">
                        <div id="inputs">
                            <br/>
                            <label>Pseudo</label><br/>
                            <input type="text" name="pseudo" class="box" required /><br /><br />
                            <label>Mot de passe</label><br/>
                            <input type="password" name="mdp" class="box" required /><br/><br />
                        </div>
                        <input type="submit" name='submit' value="Valider" class='submit'/><br />
                        <div align="center"><a href="<?=BASE_URL?>index.php/Users/oubliMDP" id="forget">Mot de passe oublié ?</a></div>
                        <?php
                        if(isset($errMsg)){
                            echo '<div id="error">'.$errMsg.'</div>';
                        }
                        ?>

                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
