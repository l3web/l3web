<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?=BASE_URL?>assets/css/formulaire.css"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/header.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/footer.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/menu_vertical.css" media="screen"/>
    <title>CollaMaps</title>
    <script type="text/javascript" src="<?=BASE_URL?>vendor/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?=BASE_URL?>assets/js/scriptJquery.js"></script>
    <script type="text/javascript" src="<?=BASE_URL?>assets/js/TypeCarte.js"></script>
</head>
<body>
    <?php
    include(VIEWS."Template/nav.php");
    include(VIEWS."Template/menu.php");
    ?>
    <div align="center" style="height:1000px;">
        <div id="cadre">
            <div id="titre">Recherche de cartes</div>
            <div id="formulaire">
                <form method="post" action="<?=BASE_URL?>index.php/carte/recherche">
                    <div id="inputs"><br/>

                        <label>Nom carte</label><br/>
                        <input type="text" name="nom_carte" value="" placeholder="nom_carte" required /><br/><br/>
                        <input type="checkbox" id="cbPub" title="cbPub" onclick="enable()"/><label>Type</label><br/>
                        <input type="text" id="typec" name="type_carte" value="" placeholder="type_carte"  disabled/><br/>

                        <br/>
                    </div>
                    <div id="Valider"><p style="text-align: center"><input type="submit" value="Rechercher"></p></div>
                </form>
            </div>

        </div>
        <?php
            if(isset($Rech['compteurcarte'])) {
                echo "<br />";
                if($Rech['recherchenom']=="notfound") {
                    echo "recherche raté";
                    unset($Rech['recherchenom']);
                }
                else {

                    for($i=0;$i<$Rech['compteurcarte'];$i++) {
                        #résultat de la recherche/lien
                        echo "Nom de la carte:" . $Rech['recherchenom'][$i] . " Type de carte : " . $Rech['recherchetype'][$i];
                        echo "<br/>";
                        echo $Rech['recherchestring'];
                        echo "<br/>";
                    }
                }
                unset($Rech['recherchenom']);
            }


        ?>
    </div>
</body>
</html>
