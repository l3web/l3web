<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?=BASE_URL?>assets/css/formulaire.css"/>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.0.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/header.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/menu_vertical.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/footer.css" media="screen"/>
    <title>CollaMaps</title>
</head>
<body>

<?php
include(VIEWS."Template/nav.php");
include(VIEWS."Template/menu.php");
?>

<div align="center" id="bloc">
    <div id="cadre">
        <div id="titre">Nouvelle carte</div>
        <div id="formulaire">
            <form method="post" action="<?=BASE_URL?>index.php/Carte/ajoutCarte"><br/>
                <div id="inputs">
                    <label>Nom de la carte</label><br/>
                    <input type="text" name="nom" value="" required/><br/><br/>
                    <label>Type de carte</label><br/>
                    <select name="type" id="type">
                        <option value="privee">Privée</option>
                        <option value="partagee">Partagée</option>
                        <option value="publique">Publique</option>
                    </select><br/><br/>
                    <div id="users" style="display: none;">
                        <label>Partager avec :</label><br/>
                        <?php
                            foreach ($users as $user) {
                                ?>
                                <input name="users[]" id="check--<?=$user['email']?>" class="name" type="checkbox" value="<?=$user['email']?>" >
                                <?=$user['email']?><br/>
                                <div align="center" >
                                <select name="roles[]" id="list--<?=$user['email']?>" class="role" style="display: none">
                                    <option value="Administrateur">Administrateur</option>
                                    <option value="Editeur">Editeur</option>
                                    <option value="Consultant">Consultant</option>
                                </select>
                                </div>
                                <hr/>
                                <?php
                            }
                        ?>
                    </div>
                </div>
                <div id="Valider"><input id="ok" type="submit" class="submit" value="Ok"></div>
                <?php
                if(isset($errMsg)){
                    echo '<div id="error">'.$errMsg.'</div>';
                }
                ?>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        //On décoche toutes les checkboxs
        $(':checkbox').prop('checked', false);

        //On rend indisponible toutes les listes pour éviter les retours non voulus
        $('.role').each(function(){
            this.disabled = "true";
        });


        if ($('option:selected', this).attr('value') == "privee") {
            $("#users").fadeOut("slow");
        } else {
            $('#users').fadeIn("slow");
        }

    });

    //On affiche les listes si l'utilisateur est coché
    $(":checkbox").change(function() {
        listId = "list--" + this.id.split("--")[1];
        if(this.checked) {
            document.getElementById(listId).style.display = "block";
            document.getElementById(listId).disabled = "";
        } else {
            document.getElementById(listId).style.display = "none";
            document.getElementById(listId).disabled = "true";
        }
    });

    //On affiche les autres utilisateurs seulement si on n'a pas choisi "privee"
    $(function() {
        $("#type").change(function(){
            if ($('option:selected', this).attr('value') == "privee") {
                $("#users").fadeOut("slow");
            } else {
                $('#users').fadeIn("slow");
            }
        });
    });
</script>
</body>
</html>
