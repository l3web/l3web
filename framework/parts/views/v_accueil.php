<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>CollaMaps</title>
    <meta charset="UTF-8" />

    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/style_accueil.css" />
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/style.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/header.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/menu_vertical.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/footer.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/formulaire.css" media="screen"/>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">

    <script type="text/javascript" src="<?=BASE_URL?>assets/js/modernizr.custom.53451.js"></script>
    <script type="text/javascript" src="<?=BASE_URL?>assets/js/jquery-1.11.2.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?=BASE_URL?>assets/js/jquery.gallery.js"></script>
    <!-- Bootstrap core CSS -->
		<!--<link href="css/bootstrap.min.css" rel="stylesheet">-->
</head>

<body>

<?php
include(VIEWS."Template/nav.php");
include(VIEWS."Template/menu.php");
?>

<div class="row">
    <!-- /.Animation -->
    <div class="col-md-10">
        <!-- Debut Anim  -->
        <div class="container">
            <!-- Codrops top bar -->
            <section id="dg-container" class="dg-container">
                <div class="dg-wrapper">
                    <a><img src="<?=BASE_URL?>assets/IMAGES/1.png"></a>
                    <a><img src="<?=BASE_URL?>assets/IMAGES/2.png"></a>
                    <a><img src="<?=BASE_URL?>assets/IMAGES/3.png"></a>



                </div>

            </section>
            <p id="textAccueil" align="center">
                Bienvenue sur CollaMaps, nous sommes heureux de votre visite !<br/>
                Vous trouverez ici tous les outils nécessaires à la réalisation de cartes heuristiques collaboratives.<br/>
                Amusez-vous bien !
            </p>
        </div>
        <!-- Fin Anim -->
    </div>
</div>


    
    <!--Javascript pour faire tourner les images-->
    <script>window.jQuery</script>
		<script type="text/javascript">
			$(function() {
				$('#dg-container').gallery({
					autoplay	:	true
				});
			});

		</script/>
			
</body>
</html>
