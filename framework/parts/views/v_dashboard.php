<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?=BASE_URL?>assets/css/formulaire.css"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/header.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/menu_vertical.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/footer.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?=BASE_URL?>assets/css/dashboard.css" media="screen"/>

    <script type="text/javascript" src="<?=BASE_URL?>vendor/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="<?=BASE_URL?>assets/js/scriptJquery.js"></script>
</head>
<body>
<?php
include(VIEWS."Template/nav.php");
include(VIEWS."Template/menu.php");
?>
<div align="center">
    <div id="cadre">
        <div id="titre">Dashboard</div>
        <div id="dashboard">
            <?php

            if (count($arrayPriv) > 0) {
                echo "<b>Les cartes privées : </b><br/>";
                for ($i = 0; $i < count($arrayPriv); $i++) {
                    echo "<a href='" . BASE_URL . "index.php/Carte/edition/" . $arrayPriv[$i]['nom_carte'] . "' class='cartes'>" . $arrayPriv[$i]['nom_carte'] . "</a>     ";

                    if (strcmp($arrayPriv[$i]['role'], "Administrateur") !== 0) {
                        echo "<a href='" . BASE_URL . "index.php/Carte/gestionCarte/" . $arrayPriv[$i]['nom_carte'] . "' class='buttonD'>Modifier</a>  ";
                    } else {
                        echo "<a href='" . BASE_URL . "index.php/Carte/gestionCarte/" . $arrayPriv[$i]['nom_carte'] . "' class='button'>Modifier</a>  ";
                    }

                    if (strcmp($arrayPriv[$i]['role'], "Administrateur") !== 0) {
                        echo "<a href='" . BASE_URL . "index.php/Carte/suppressionCarte/" . $arrayPriv[$i]['nom_carte'] . "' class='buttonD'
                        onclick='confirm(\"Voulez-vous vraiment supprimer cette carte ?\")'>Supprimer</a>";
                        echo "<br/>";
                    } else {
                        echo "<a href='" . BASE_URL . "index.php/Carte/suppressionCarte/" . $arrayPriv[$i]['nom_carte'] . "' class='button'
                        onclick='confirm(\"Voulez-vous vraiment supprimer cette carte ?\")'>Supprimer</a>";
                        echo "<br/>";
                    }
                }
                echo "<br/>";
            }

            if (count($arrayPart) > 0) {
                echo "<b>Les cartes partagées : </b><br/>";
                for ($i = 0; $i < count($arrayPart); $i++) {
                    echo "<a href='" . BASE_URL . "index.php/Carte/edition/" . $arrayPart[$i]['nom_carte'] ."' class='cartes'/>" . $arrayPart[$i]['nom_carte'] . "</a>     ";

                    if (strcmp($arrayPart[$i]['role'], "Administrateur") !== 0) {
                        echo "<a href='" . BASE_URL . "index.php/Carte/gestionCarte/" . $arrayPart[$i]['nom_carte'] . "' class='buttonD'>Modifier</a>     ";
                    } else {
                        echo "<a href='" . BASE_URL . "index.php/Carte/gestionCarte/" . $arrayPart[$i]['nom_carte'] . "' class='button'>Modifier</a>     ";
                    }

                    if (strcmp($arrayPart[$i]['role'], "Administrateur") !== 0) {
                        echo "<a href='" . BASE_URL . "index.php/Carte/suppressionCarte/" . $arrayPart[$i]['nom_carte'] . "' class='buttonD'
                        onclick='confirm(\"Voulez-vous vraiment supprimer cette carte ?\")'>Supprimer</a>";
                        echo "<br/>";
                    } else {
                        echo "<a href='" . BASE_URL . "index.php/Carte/suppressionCarte/" . $arrayPart[$i]['nom_carte'] . "' class='button'
                        onclick='confirm(\"Voulez-vous vraiment supprimer cette carte ?\")'>Supprimer</a>";
                        echo "<br/>";
                    }
                }
                echo "<br/>";
            }

            if (count($arrayPub) > 0) {
                echo "<b>Vos cartes publiques : </b><br/>";
                for ($i = 0; $i < count($arrayPub); $i++) {
                    echo "<a href='" . BASE_URL . "index.php/Carte/edition/" . $arrayPub[$i]['nom_carte'] . "' class='cartes'/>" . $arrayPub[$i]['nom_carte'] . "</a>     ";

                    if (strcmp($arrayPub[$i]['role'], "Administrateur") !== 0) {
                        echo "<a href='" . BASE_URL . "index.php/Carte/gestionCarte/" . $arrayPub[$i]['nom_carte'] . "' class='buttonD'>Modifier</a>     ";
                    } else {
                        echo "<a href='" . BASE_URL . "index.php/Carte/gestionCarte/" . $arrayPub[$i]['nom_carte'] . "' class='button'>Modifier</a>     ";
                    }


                    if (strcmp($arrayPub[$i]['role'], "Administrateur") !== 0) {
                        echo "<a href='" . BASE_URL . "index.php/Carte/suppressionCarte/" . $arrayPub[$i]['nom_carte'] . "' class='buttonD'
                        onclick='confirm(\"Voulez-vous vraiment supprimer cette carte ?\")'>Supprimer</a>";
                        echo "<br/>";
                    } else {
                        echo "<a href='" . BASE_URL . "index.php/Carte/suppressionCarte/" . $arrayPub[$i]['nom_carte'] . "' class='button'
                        onclick='confirm(\"Voulez-vous vraiment supprimer cette carte ?\")'>Supprimer</a>";
                        echo "<br/>";
                    }
                }
                echo "<br/>";
            }

            if (count($allPub) > 0) {
                echo "<b>Toutes les cartes publiques : </b><br/>";
                for ($i = 0; $i < count($allPub); $i++) {
                    echo "<a href='" . BASE_URL . "index.php/Carte/edition/" . $allPub[$i]['nom_carte'] . "' class='cartes'/>" . $allPub[$i]['nom_carte'] . "</a>     ";
                    echo "<br/>";
                }
            }

            if (count($arrayPriv) + count($arrayPub) + count($arrayPart) == 0) {
                echo "<h3 style='text-align : center'>
                    Vous n'avez aucune carte<br/>
                    Créez en une
                    <a href ='".BASE_URL."index.php/Carte/creationCarte' class='cartes'>ici</a></h3>";
            }

            ?>
        </div>
    </div>
</div>
</body>
</html>