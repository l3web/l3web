<?php
class m_Carte {
    public $db;

    function __construct() {
        $this->db = Database::getInstance();
    }

	function recherche($nomCarte,$typeCarte){

    $requete = "SELECT * FROM carte WHERE nom_carte LIKE ? AND type_carte=?";
    $param = "%" . $nomCarte . "%";
    $resultat = $this->db->prepare($requete);
    $resultat->execute(array($param,$typeCarte));

    return $resultat;
}

    function rechercheN($nomCarte){

        $requete = "SELECT * FROM carte WHERE nom_carte LIKE ?";
        $param = "%" . $nomCarte . "%";
        $resultat = $this->db->prepare($requete);
        $resultat->execute(array($param));

        return $resultat;
    }

    function newCarte($nom, $type, $usersArray, $rolesArray) {
        $requete = 'INSERT INTO carte(nom_carte, type_carte) VALUES (?,?)';
        $insert = $this->db->prepare('INSERT INTO carte(nom_carte, type_carte) VALUES (?,?)');
        $insert->execute(array($nom, $type));

        for($i= 0; $i < count($usersArray); $i++) {
            $requete = "INSERT INTO user_list VALUES (?,?,?)";
            $insert = $this->db->prepare($requete);
            $insert->execute(array($nom, $usersArray[$i], $rolesArray[$i]));
        }
    }

    function checkNewCarte($nom) {
        $requete = "SELECT * FROM carte WHERE nom_carte = ?";
        $select = $this->db->prepare($requete);
        $select->execute(array($nom));
        return ($select->rowCount() == 0);
    }

    function getCarte($nom_carte) {
        $requete = "SELECT * FROM carte WHERE nom_carte = ?";
        $select = $this->db->prepare($requete);
        $select->execute(array($nom_carte));

        return $select->rowCount();
    }

    function updateCarte($ancien_nom, $nouveau_nom, $type,$users, $roles) {
        try {
            $requete = "DELETE FROM user_list WHERE nom_carte = ?";
            $delete = $this->db->prepare($requete);
            $delete->execute(array($ancien_nom));

            $requete = "UPDATE carte SET
                        nom_carte = ?,
                        type_carte = ?
                        WHERE nom_carte = ?";
            $update = $this->db->prepare($requete);
            $update->execute(array($nouveau_nom, $type, $ancien_nom));



            for ($i = 0; $i < count($users); $i++) {
                $requete = "INSERT INTO user_list VALUES (?,?,?)";
                $insert = $this->db->prepare($requete);
                $insert->execute(array($nouveau_nom, $users[$i], $roles[$i]));
            }
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    function checkRoleCarte($nom_carte, $email) {
        $requete = "SELECT role FROM user_list WHERE nom_carte = ? AND email_utilisateur = ?";
        $select = $this->db->prepare($requete);
        $select->execute(array($nom_carte, $email));

        if ($select->rowCount() == 0) {
            return null;
        } else {
            return $select->fetchAll()[0][0];
        }
    }

    function getTypeCarte($nom_carte) {
        $requete = "SELECT type_carte FROM carte WHERE nom_carte = ?";
        $select = $this->db->prepare($requete);
        $select->execute(array($nom_carte));
        if ($select->rowCount()== 0) {
            return null;
        }
        return $select->fetchAll()[0][0];
    }

    function getSVG($nom_carte) {
        $requete = "SELECT string_carte FROM carte WHERE nom_carte = ?";
        $select = $this->db->prepare($requete);
        $select->execute(array($nom_carte));
        if ($select->rowCount()== 0) {
            return null;
        }
        return $select->fetchAll()[0][0];
    }

    function rechercheCarte($mail){

        $requete = "SELECT carte.nom_carte, carte.type_carte, user_list.role FROM carte, user_list WHERE user_list.email_utilisateur=? AND carte.nom_carte = user_list.nom_carte";
        $resultat = $this->db->prepare($requete);

        $resultat->execute(array($mail));

        return $resultat;
    }
    function rechercheTypeCarte($nomcarte){

        $requete = "SELECT * FROM carte WHERE nom_carte=?";
        $resultat = $this->db->prepare($requete);

        $resultat->execute(array($nomcarte));

        return $resultat;
    }

    function updateSVGCarte($nom,$svg){
        $requete = "UPDATE carte SET `string_carte` = '$svg' where nom_carte = '$nom'";
        $resultat = $this->db->prepare($requete);
        $resultat->execute(array($nom,$svg));

        return $resultat;

    }

    function suppressionCarte($nom_carte) {
        $requete1 = "DELETE FROM user_list WHERE nom_carte = ?";
        $requete2 = "DELETE FROM carte WHERE nom_carte = ?";

        $delete1 = $this->db->prepare($requete1);
        $delete1->execute(array($nom_carte));

        $delete1 = $this->db->prepare($requete2);
        $delete1->execute(array($nom_carte));
    }


    function doBusy($nom){
        $requete = "UPDATE carte SET busy = 1 where nom_carte = '$nom'";
        $resultat = $this->db->prepare($requete);
        $resultat->execute(array($nom,1));

        return $resultat;
    }



    function undoBusy($nom){
        $requete = "UPDATE carte SET busy = 0 where nom_carte = '$nom'";
        $resultat = $this->db->prepare($requete);
        $resultat->execute(array($nom,0));

        return $resultat;
    }

    function getBusy($nom_carte) {
        $requete = "SELECT busy FROM carte WHERE nom_carte = ?";
        $select = $this->db->prepare($requete);
        $select->execute(array($nom_carte));
        if ($select->rowCount()== 0) {
            return null;
        }
        return $select->fetchAll()[0][0];
    }

    function rechercheCartesPubliques() {
        $requete = "SELECT DISTINCT(carte.nom_carte) FROM user_list, carte WHERE carte.nom_carte = user_list.nom_carte
          AND carte.type_carte = 'Publique'";
        $select = $this->db->prepare($requete);
        $select->execute();

        if ($select->rowCount() == 0) return null;
        return $select->fetchAll();
    }

}
