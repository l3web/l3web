<?php
class m_Users {
    public $db;

	function __construct() {
        $this->db = Database::getInstance();
	}

	function connexion($pseudo,$mdp){
		$requete = "SELECT * FROM utilisateur WHERE pseudo=? AND mdp=?";
        $resultat = $this->db->prepare($requete);

		$resultat->execute(array($pseudo,$mdp));		
		//var_dump($resultat);
		return $resultat;
	}

    public function inscription($email, $pseudo, $mdp, $nom, $prenom, $confirmKey){
        try {
            $req = $this->db->prepare('
                INSERT INTO utilisateur(
                email,
                pseudo,
                mdp,
                confirmKey,
                valide,
                nom,
                prenom,
                id_droit,
                date_inscription
                ) VALUES (
                ?,?,?,?,false,?,?,\'Client\',
                NOW())'
            );
            $req->execute(array(
                $email,
                $pseudo,
                md5($mdp),
                $confirmKey,
                $nom,
                $prenom
            ));

            $stmt = $this->db->prepare("SELECT * FROM utilisateur");
            if ($stmt->execute(array($pseudo))) {
                while ($row = $stmt->fetch()) {
                    //print_r($row);
                }
            }
        } catch (PDOException $e) {
            echo "<p>Erreur : " . $e->getMessage() . "</p>";
            exit();
        }
    }

    public function validInscription($email, $key) {
        $requete = "SELECT confirmKey, valide FROM utilisateur where email = ?";
        $exec = $this->db->prepare($requete);
        $exec->execute(array($email));

        $result = $exec->fetchAll();

        $confirmKey = $result[0][0];
        $valide = $result[0][1];
        if ($valide == true) {
            return "Votre compte a déjà été activé";
        }

        if ($confirmKey == $key) {
            $requete = "UPDATE utilisateur SET valide = 1 where email = ?";
            $exec = $this->db->prepare($requete);
            $exec->execute(array($email));
            return "Votre compte est maintenant activé";
        } else {
            return "Clé de validation invalide";
        }
    }
    public function validInscriptionAuto($email) {
        $requete = "UPDATE utilisateur SET valide = '1' WHERE email = ?";
        $update = $this->db->prepare($requete);
        $update->execute(array($email));
    }


    //recupération des noms d'utilisateurs
    public function recupUtilisateurs()
    {
        $tab=array();
        $i=0;
        $quer="SELECT * FROM utilisateur WHERE id_droit<>'Super-Admin'" ;
        $result = $this->db->query($quer) or exit(print_r($this->db->errorInfo()));
        while($data = $result->fetch(PDO::FETCH_OBJ))
        {
            $tab[$i]=$data;$i++;
        }
        return $tab;

    }
    //recup�ration des emails bannis
    public function recupBannis($mail)
    {
        $tab=array();
        $i=0;
        $quer="SELECT * FROM bannis WHERE email_banni='$mail' AND date_debut<>'0000-00-00' AND date_fin='0000-00-00'" ;
        $result = $this->db->query($quer) or exit(print_r($this->db->errorInfo()));
        while($data = $result->fetch(PDO::FETCH_OBJ))
        {
            $tab[$i]=$data;
            $i++;
        }
        return $tab;
    }
    //liste des personnes bannis
    public function listeBannis()
    {
        $tab=array();
        $i=0;
        $quer="SELECT * FROM bannis WHERE date_debut<>'0000-00-00' AND date_fin='0000-00-00'" ;
        $result = $this->db->query($quer) or exit(print_r($this->db->errorInfo()));
        while($data = $result->fetch(PDO::FETCH_OBJ))
        {
            $tab[$i]=$data;
            $i++;
        }
        return $tab;
    }
    //historique des personnes bannis
    public function histoBannis($mail)
    {
        $tab=array();
        $i=0;
        $quer="SELECT * FROM bannis WHERE email_banni='$mail'" ;
        $result = $this->db->query($quer) or exit(print_r($this->db->errorInfo()));
        while($data = $result->fetch(PDO::FETCH_OBJ))
        {
            $tab[$i]=$data;
            $i++;
        }
        return $tab;
    }

    //mise � jour de la table bannis
    public function insertBannis($mail,$raison)
    {
        $date=CURRENT_TIMESTAMP ;
        $quer="INSERT INTO bannis(id_ban,email_banni,date_debut,date_fin,raison) VALUES('','$mail',CURRENT_DATE,'','$raison') " ;
        $result = $this->db->query($quer) or exit(print_r($this->db->errorInfo()));

    }

    //mise � jour de la table bannis
    public function updateBannis($mail)
    {
        $quer="UPDATE bannis SET date_fin=CURRENT_DATE WHERE email_banni='$mail' " ;
        $result = $this->db->query($quer) or exit(print_r($this->db->errorInfo()));


    }
    //mise � jour de la table utilisateur
    public function updateUtilisateur($mail,$droit)
    {
        $quer="UPDATE utilisateur SET id_droit='$droit' WHERE email='$mail' " ;
        $result = $this->db->query($quer) or exit(print_r($this->db->errorInfo()));


    }
    
    public function updateNom($nom,$email)
    {
		$requete='update utilisateur set  nom=? where email=?';
		$resultat = $this->db->prepare($requete);
		$resultat->execute(array($nom,$email));
	}
	
    public function updatePrenom($prenom,$email)
    {
		$requete='update utilisateur set  prenom=? where email=?';
		$resultat = $this->db->prepare($requete);
		$resultat->execute(array($prenom,$email));
	}
    public function updatePseudo($pseudo,$email)
    {
		$requete='update utilisateur set  pseudo=? where email=?';
		$resultat = $this->db->prepare($requete);
		$resultat->execute(array($pseudo,$email));
	}
    public function updateMdp($mdp,$email)
    {
		$requete='update utilisateur set  mdp=? where email=?';
		$resultat = $this->db->prepare($requete);
		$resultat->execute(array(md5($mdp),$email));
	}

    public function getInfosUtilisateur($email) {
        $requete = "SELECT * FROM utilisateur WHERE email = ?";
        $select = $this->db->prepare($requete);
        $select->execute(array($email));
        $result = $select->fetchAll();
        return $result;
    }

    public function getAllOtherUsers($email) {
        $requete = "SELECT * FROM utilisateur where email != ?";
        $select = $this->db->prepare($requete);
        $select->execute(array($email));
        $users = $select->fetchAll();
        return $users;
    }

    public function getCurrentBanReason($email) {
        $requete = "SELECT raison FROM bannis WHERE date_fin ='0000-00-00' AND email_banni =?";
        $select = $this->db->prepare($requete);
        $select->execute(array($email));
        if ($select->rowCount() ==0) return null;
        return $select->fetchAll()[0][0];
    }
}

?>
