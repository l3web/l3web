<?php

class Carte
{
    //Index
    public function index()
    {
        include(VIEWS . "v_recherche.php");
    }

    function recherche()
    {
        if (isset($_POST['nom_carte']) && isset($_POST['type_carte'])) {

            if ($_POST['nom_carte'] == '' AND $_POST['type_carte'] == '') {
                $this->errMsg .= 'Vous devez remplir les deux champs<br>';
                echo "<p>Vous devez remplir les champs</p>";
            }

            include(MODELS . "m_carte.php");

            $m_carte = new m_Carte();
            $resultat = $m_carte->recherche($_POST['nom_carte'], $_POST['type_carte']);

            if ($resultat->rowCount() >= 1) {
                $Rech = array('recherchenom', 'compteurcarte', 'recherchetype', 'recherchestring');
                $Rech['recherchenom'] = array();
                $Rech['recherchetype'] = array();
                $Rech['recherchestring'] = array();
                $Rech['compteurcarte'] = 0;
                $result = $resultat->fetchAll();
                for ($j = 0; $j < $resultat->rowCount(); $j++) {
                    $Rech['compteurcarte'] = $Rech['compteurcarte'] + 1;
                    $Rech['recherchenom'][$j] = $result[$j]['nom_carte'];
                    $Rech['recherchetype'][$j] = $result[$j]['type_carte'];
                    $Rech['recherchestring'][$j] = $result[$j]['string_carte'];
                }
                include(VIEWS . "v_recherche.php");

            } else {
                $Rech['recherchenom'] = "notfound";
                include(VIEWS . "v_recherche.php");
            }
        } else {
            if ($_POST['nom_carte'] == '') {
                $this->errMsg .= 'Vous devez remplir le champs<br>';
                echo "<p>Vous devez remplir le champs</p>";
            }

            include(MODELS . "m_carte.php");

            $m_carte = new m_Carte();
            $resultat = $m_carte->rechercheN($_POST['nom_carte']);

            if ($resultat->rowCount() >= 1) {


                $Rech = array('recherchenom', 'compteurcarte', 'recherchetype', 'recherchestring');
                $Rech['recherchenom'] = array();
                $Rech['recherchetype'] = array();
                $Rech['recherchestring'] = array();
                $Rech['compteurcarte'] = 0;
                $result = $resultat->fetchAll();

                for ($i = 0; $i < $resultat->rowCount(); $i++) {
                    $Rech['compteurcarte'] = $Rech['compteurcarte'] + 1;
                    $Rech['recherchenom'][$i] = $result[$i]['nom_carte'];
                    $Rech['recherchetype'][$i] = $result[$i]['type_carte'];
                    $Rech['recherchestring'] = $result[$i]['string_carte'];
                }
                include(VIEWS . "v_recherche.php");

            } else {
                $Rech['recherchenom'] = "notfound";
                include(VIEWS . "v_recherche.php");
            }
        }
    }

    public function edition($nom_carte = '')
    {
        include(MODELS . "m_carte.php");
        $m_carte = new m_Carte();

        $type = $m_carte->getTypeCarte($nom_carte);
        $string_carte = $m_carte->getSVG($nom_carte);

        if (is_null($type)) {
            include(VIEWS . "v_accueil.php");
            include(VIEWS . "Template/footer.php");
            ?>
            <script>alert("Carte inexistante.")</script>
            <?php
            return;
        }

        $role = $m_carte->checkRoleCarte($nom_carte, $_SESSION['email']);

        //Si la carte n'est pas publique et qu'on y a pas accès
        if (strcmp($type, "Publique") !== 0 && is_null($role)) {
            include(VIEWS . "v_accueil.php");
            include(VIEWS . "Template/footer.php");
            ?>
            <script>alert("Vous n'avez pas le droit d'accès à cette carte.")</script>
            <?php
            return;

            //Si la carte est publique et qu'on est pas dans sa user_list, on est consultant
        } else if (strcmp($type, "Publique") == 0 && is_null($role)) {
            $_SESSION["role"]["$nom_carte"] = "Consultant";

            //Si on a accès à la carte
        } else {
            $_SESSION["role"]["$nom_carte"] = $role;
        }

        if ($m_carte->getBusy($nom_carte) == 1) {
            ?>
            <script>alert("Cette carte est occupée.")</script>
            <?php
            include(VIEWS . "v_accueil.php");
            include(VIEWS . "Template/footer.php");
            return;
        } else {
            if (strcmp($_SESSION["role"]["$nom_carte"], "Consultant") !== 0) {
                $m_carte->doBusy($nom_carte);
            }
        }

        include(VIEWS . "v_edition.php");
        include(VIEWS . "Template/footer.php");

    }

    public function creationCarte($error = '')
    {
        if (!isset($_SESSION['email'])) {
            $this->redirectTo();
            return;
        }

        include(MODELS . "m_users.php");
        $m_user = new m_Users();
        $users = $m_user->getAllOtherUsers($_SESSION['email']);
        if (isset($error)) {
            $errMsg = $error;
        }
        include(VIEWS . "v_creationCarte.php");
        include(VIEWS . "Template/footer.php");
    }

    public function ajoutCarte()
    {
        if (!isset($_SESSION['id_droit'])) {
            $this->redirectTo();
            return;
        }

        include_once(MODELS . "m_carte.php");
        $m_carte = new m_Carte();

        if (!$m_carte->checkNewCarte($_POST['nom'])) {
            $error = "Une carte avec le nom " . $_POST['nom'] . " existe déjà.";
            $this->creationCarte($error);
            return;
        }

        $type_carte = "";
        $users = array();
        $roles = array();

        if (isset($_POST['users'])) {
            $users = $_POST['users'];
        }
        if (isset($_POST['roles'])) {
            $roles = $_POST['roles'];
        }

        array_push($roles, "Administrateur");
        array_push($users, $_SESSION['email']);

        if ($_POST['type'] == "privee") {
            $type_carte = "Privee";
        } else if ($_POST['type'] == "publique") {
            $type_carte = "Publique";
        } else {
            $type_carte = "Partagee";
        }

        $m_carte->newCarte($_POST['nom'], $type_carte, $users, $roles);
        header("LOCATION: " . BASE_URL . "index.php/Carte/edition/" . $_POST['nom']);

    }

    public function gestionCarte($nom_carte = '', $errMsgForm = '')
    {
        if (!isset($_SESSION['id_droit'])) {
            $this->redirectTo();
            return;
        }

        if (empty($nom_carte)) {
            $this->index();
        } else {
            $errMsg = "";
            $errMsgForm = $errMsgForm;
            include_once(MODELS . "m_carte.php");
            $m_carte = new m_Carte();

            include(MODELS . "m_users.php");
            $m_user = new m_Users();
            $users = $m_user->getAllOtherUsers($_SESSION['email']);

            if ($m_carte->getCarte($nom_carte) == 0) {
                $errMsg = "La carte $nom_carte n'existe pas";
            }
            include(VIEWS . "v_gestionCarte.php");
            include(VIEWS . "Template/footer.php");
        }
    }

    public function updateCarte($ancien_nom = '')
    {
        include_once(MODELS . "m_carte.php");
        $m_carte = new m_Carte();
        $errMsgForm = "";
        $type_carte = "";
        $users = array();
        $roles = array();

        if (isset($_POST['users'])) {
            $users = $_POST['users'];
        }
        if (isset($_POST['roles'])) {
            $roles = $_POST['roles'];
        }

        array_push($roles, "Administrateur");
        array_push($users, $_SESSION['email']);

        if ($_POST['type'] == "privee") {
            $type_carte = "Privee";
        } else if ($_POST['type'] == "publique") {
            $type_carte = "Publique";
        } else {
            $type_carte = "Partagee";
        }
        $succes = $m_carte->updateCarte($ancien_nom, $_POST['nom'], $type_carte, $users, $roles);

        if ($succes) {
            ?>
            <script type="text/javascript">
                alert("La carte a été modifiée avec succès.");
            </script>
            <?php
            include_once(CONTROLLERS . "c_dashboard.php");
            $dash = new DashBoard();
            $dash->index();
        } else {
            $errMsg = "";
            $errMsgForm = "Ce nom est déjà utilisé par une autre carte";
            $nom_carte = $_POST['nom'];

            include_once(MODELS . "m_users.php");
            $m_user = new m_Users();
            $users = $m_user->getAllOtherUsers($_SESSION['email']);

            include(VIEWS . "v_gestionCarte.php");
            include(VIEWS . "Template/footer.php");
        }
    }

    public function saveinDb($nom_carte)
    {
        include(MODELS . "m_carte.php");
        header("X-XSS-Protection: 0");
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

        $data = array();
        $data[0] = $nom_carte;
        if (!empty($_POST['smthg']))
            $data[1] = $_POST['smthg'];

        $m_carte = new m_Carte();
        $search_Result = $m_carte->rechercheTypeCarte($data[0]);

        if ($data[1] != "nope") {
            if ($search_Result) {
                $m_carte->updateSVGCarte($data[0], $data[1]);
                ?>
                <script>alert("Carte enregistrée")</script><?php
            } else {
                ?>
                <script>alert("Carte non enregistrée")</script><?php
            }
        } else {
            ?>
            <script>alert("Carte non enregistrée")</script><?php
        }

        $m_carte->undoBusy($nom_carte);

        include(VIEWS . "v_accueil.php");
        include(VIEWS . "Template/footer.php");
    }

    public function suppressionCarte($nom_carte = '')
    {
        var_dump($_SESSION['id_droit']);
        if (!isset($_SESSION['id_droit'])) {
            $this->redirectTo();
            return;
        }

        include(MODELS . "m_carte.php");
        $m_carte = new m_Carte();

        $role = $m_carte->checkRoleCarte($nom_carte, $_SESSION['email']);

        if (strcmp($role, 'Administrateur') !== 0) {
            $this->redirectTo();
            return;
        }

        $m_carte->suppressionCarte($nom_carte);

        include_once(CONTROLLERS . "c_dashboard.php");
        $dash = new DashBoard();
        $dash->index();
        return;

    }

    //Redirige vers le path passé en paramètre et vers l'index par défaut
    private function redirectTo($path = '')
    {
        if (!headers_sent()) {
            header("Location: " . BASE_URL . "index.php/" . $path);
        } else {
            ?>
            <script type="text/javascript">
                document.location.href = "<?=BASE_URL . $path?>";
            </script>
            <?php
        }
    }
}
