<?php

class DashBoard
{
        //Index
        public function index()
        {
            include_once(MODELS . "m_carte.php");

            $m_dashboard = new m_Carte();

            if (!isset($_SESSION['id_droit'])) {
                include(VIEWS . "v_accueil.php");
                include(VIEWS . "Template/footer.php");
                return;
            }

            $resultat = $m_dashboard->rechercheCarte($_SESSION['email']);
            $tab = $resultat->fetchAll();

            $arrayPub = array();
            $arrayPart = array();
            $arrayPriv = array();

            $allPub = $m_dashboard->rechercheCartesPubliques();

            if ($resultat->rowCount() >= 1) {

                for ($i = 0; $i < count($tab); $i++) {
                    if ($tab[$i]['type_carte'] == "Publique") {
                        array_push($arrayPub, $tab[$i]);
                    } else if($tab[$i]['type_carte'] == "Partagee") {
                        array_push($arrayPart, $tab[$i]);
                    } else {
                        array_push($arrayPriv, $tab[$i]);
                    }
                }
            }

            include(VIEWS . "v_dashboard.php");
            include(VIEWS."Template/footer.php");
        }
    }