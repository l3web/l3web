<?php
class Users {

	function index(){
		include(VIEWS.'v_accueil.php');
		include(VIEWS."Template/footer.php");
	}

    function signIn() 
    {
        if (!empty($_SESSION['pseudo'])) {
            $this->index();
            return;
        }
        include(VIEWS."v_connexion.php");
        include(VIEWS."Template/footer.php");
    }

    function signUp() 
    {
        if (!empty($_SESSION['pseudo'])) {
            $this->index();
            return;
        }
        include(VIEWS."v_inscription.php");
        include(VIEWS."Template/footer.php");
    }

    function gestionProfil()
    {
        if (empty($_SESSION['pseudo'])) {
            $this->index();
            return;
        }
        include(VIEWS."v_profil.php");
        include(VIEWS."Template/footer.php");
	}

	function sessionConnexion() {
        $errMsg='';
		if (isset($_POST['pseudo']) && isset($_POST['mdp'])) {
	
		    if($_POST['pseudo'] == '' AND  $_POST['mdp'] == ''){
			    $this->errMsg .= 'Vous devez remplir les deux champs<br>';
            }
		    include(MODELS.'m_users.php');
		    $m_user = new m_Users();

		    $resultat=$m_user->connexion($_POST['pseudo'],md5($_POST['mdp']));
		
            if($resultat->rowCount()==1){

                $result = $resultat->fetchAll();

                if ($result[0]['valide'] == 0) {
                    $errMsg = "Votre compte n'est pas activé.";
                    include(VIEWS . 'v_connexion.php');
                    include(VIEWS . "Template/footer.php");
                    return;
                }

                $ban = $m_user->getCurrentBanReason($result[0]['email']);

                if ( sizeof($ban)>0) {
                    $errMsg = "Vous avez été banni pour la raison suivante : <br/>".$ban;
                    include(VIEWS . 'v_connexion.php');
                    include(VIEWS . "Template/footer.php");
                    return;
                } else {
                    $_SESSION['pseudo'] = $_POST['pseudo'];
                    $_SESSION['mdp'] = $_POST['mdp'];
                    $_SESSION['id_droit'] = $result[0]['id_droit'];
                    $_SESSION['email'] = $result[0]['email'];
                    $_SESSION['date_inscription'] = $result[0]['date_inscription'];
                    $_SESSION['prenom'] = $result[0]['prenom'];
                    $_SESSION['nom'] = $result[0]['nom'];

                    include(VIEWS . 'v_accueil.php');
                    include(VIEWS . "Template/footer.php");
                }
		    } else {
			    $errMsg = "Identifiants incorrects";
                include(VIEWS.'v_connexion.php');
                include(VIEWS . "Template/footer.php");
			}
	    }
    }

    function inscription() {
        if (!empty($_SESSION['pseudo'])) {
            $this->index();
            return;
        }
        $errMsg="";
        $erreur = false;

        if (!isset($_POST['pseudo'])) {
            $errMsg .= "Veuillez indiquer votre pseudo<br/>";
        }

        if (!isset($_POST['nom'])) {
            $errMsg .= "Veuillez indiquer votre nom<br/>";
        }

        if (!isset($_POST['prenom'])) {
            $errMsg .= "Veuillez indiquer votre prénom<br/>";
        }

        if (!isset($_POST['email'])) {
            $errMsg .= "Veuillez indiquer votre adresse mail<br/>";
        }

        if (!isset($_POST['mdp'])) {
            $errMsg .= "Veuillez indiquer votre mot de passe<br/>";
        }

        include(MODELS.'m_users.php');
        $m_user = new m_Users();

        if (strlen($errMsg) == 0) {

            //Génération de la clé de confirmation
            $confirmKey = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 12)), 0, 12);

            $m_user->inscription($_POST['email'], $_POST['pseudo'], $_POST['mdp'], $_POST['nom'], $_POST['prenom'], $confirmKey);



            try {
                $this->envoiMail($_POST['email'], $confirmKey);
                ?>
                <script type="text/javascript">
                    alert("Votre compte a bien été créé.\n" +
                    "Un mail de confirmation vous a été envoyé afin d'activer votre compte.");
                </script>
            <?php
            } catch (Exception $e) {
                $m_user->validInscriptionAuto($_POST['email']);
                ?>
                <script type="text/javascript">
                    alert("Votre compte a bien été créé et validé automatiquement.");
                </script>
            <?php
            }
			include(VIEWS.'v_accueil.php');
			include(VIEWS."Template/footer.php");
        }
    }

    function gestionUsers() {
        if (empty($_SESSION['pseudo']) || $_SESSION['id_droit'] == 'Client') {
            $this->index();
            return;
        }
        if (!isset($_SESSION['id_droit']) || $_SESSION['id_droit'] == "Client"){
            echo 'pas le droit';
        } elseif ($_SESSION['id_droit'] == "Admin") {
            include(MODELS."m_users.php");
            $m_user = new m_Users();
            $tab = $m_user->recupUtilisateurs();
            include(VIEWS."v_gestionUsersAdmin.php");
        } else {
            include(MODELS."m_users.php");
            $m_user = new m_Users();
            $tab = $m_user->recupUtilisateurs();
            include(VIEWS."v_gestionUsers.php");
        }
		include(VIEWS."Template/footer.php");
    }

	function banUsers(){
        if (empty($_SESSION) || $_SESSION['id_droit'] == 'Client') {
            $this->index();
            return;
        }
		include(MODELS."m_users.php");
		$m_user = new m_Users();
		if(isset($_POST['mail'])){
			$val=$_POST['val'];
			$mail=$_POST['mail'];
			$raison=$_POST['raison'];
			if ($val=='banni') {
				$tab = $m_user->insertBannis($mail,$raison);
			}else{
				$tab = $m_user->updateBannis($mail);
			}
		}
		//include(VIEWS."v_gestionUsers.php");
	}
    //listes des personnes bannis
	function listBan(){
        if (!isset($_SESSION['pseudo'])) {
            $this->index();
            return;
        }

		include(VIEWS."Template/nav.php");
		include(VIEWS."Template/menu.php");
		include(MODELS."m_users.php");
		$m_user = new m_Users();
		$tab = $m_user->listeBannis();
		include(VIEWS."listeBanni.php");
		include(VIEWS."Template/footer.php");
	}
    function histo_ban(){
        include(MODELS."m_users.php");
              $m_user = new m_Users();
          $mail=$_POST['mail'];
        if(isset($_POST['mail'])) {
            $tab = $m_user->histoBannis($mail);
            include(VIEWS."historique.php");
           }
                }
    //historiques des personnes bannis

	function droitUsers(){
        if (empty($_SESSION['id_droit']) || $_SESSION['id_droit'] == 'Client') {
            $this->index();
            return;
        }
		include(MODELS."m_users.php");
		$m_user = new m_Users();
		if (isset($_POST['droit'])) {
			$droit=$_POST['droit'];
			$mail=$_POST['mail'];
			$tab = $m_user->updateUtilisateur($mail,$droit);

		}
	}
    
    function deconnexion()
    {
        if (empty($_SESSION)) {
            $this->index();
            return;
        }
		session_destroy();
        session_start();
		//include(VIEWS."Template/nav.php");
		//include(VIEWS."Template/menu.php");
		include(VIEWS."v_accueil.php");		
		include(VIEWS."Template/footer.php");	
	}

    function modifierProfil()
    {
        if (empty($_SESSION['pseudo'])) {
            $this->index();
            return;
        }
		include(MODELS."m_users.php");
        $m_user = new m_Users();
         
		$nom=$_POST['nom'];
		$prenom=$_POST['prenom'];
		$pseudo=$_POST['pseudo'];
		$pass=$_POST['pass'];
		$npass=$_POST['npass'];
	
		if(isset($pass) && isset($npass))
		{
			if($pass!='' && $npass!='' && $pass==$npass)
			{
				try
				{
					$m_user->updateMdp($pass,$_SESSION['email']);
					$_SESSION['mdp']=$pass;
				}
				catch (PDOException $e){
									
				}
			}
		}
		
		if(isset($nom))
		{
			if($nom!='')
			{
				try
				{
					$m_user->updateNom($nom,$_SESSION['email']);
					$_SESSION['nom']=$nom;
				}
				catch(PDOException $e){

                }
			}
		}
		
		if(isset($prenom))
		{
			if($prenom!='')
			{
				try
				{
					$m_user->updatePrenom($prenom,$_SESSION['email']);
					$_SESSION['prenom']=$prenom;
				}
				catch(PDOException $e){

                }
			}
		}
		
		if(isset($pseudo))
		{
			if($pseudo!='')
			{
				try
				{
					$m_user->updatePseudo($pseudo,$_SESSION['email']);
					$_SESSION['pseudo']=$pseudo;
				}
				catch(PDOException $e){

                }
			}
		}
		
		?>
		<script>alert("Modification réussie!")</script>
		
		<?php
		//include(VIEWS."Template/nav.php");
		//include(VIEWS."Template/menu.php");
		include(VIEWS.'v_profil.php');
		include(VIEWS."Template/footer.php");
	}

    function envoiMail($adresse, $key) {
        if (!empty($_SESSION['pseudo'])) {
            $this->index();
            return;
        }
        $header = "MIME-Version: 1.0\r\n";
        $header .= 'From:"Collamaps"<no-reply@collamaps.fr>'."\n";
        $header .= 'Content-Type:text/html; charset="utf-8"'."\n";
        $header .= 'Content-Transfer-Encoding: 8bit';

        $message = '
            <html>
                <body>
                    <div>
                        Bonjour,<br/>
                        Votre compte a bien été créé.<br/>
                        Veuillez finaliser votre inscription en cliquant
                        <a href="'.BASE_URL.'index.php/Users/validInscription/'
            .$adresse.'/'.$key.'"
                        >
                        ICI.</a>
                        <br/>
                        <br/>
                        Cordialement,<br/>
                        L\'équipe de CollaMaps
                    </div>
                </body>
            </html>
        ';

        mail($adresse, "CollaMaps - Confirmation", $message, $header);
    }

    function mailMDP() {
        if (!empty($_SESSION['pseudo'])) {
            $this->index();
            return;
        }
        $email = $_POST['email'];

        include(MODELS."m_users.php");
        $m_user = new m_Users();
        $infos = $m_user->getInfosUtilisateur($email);

        if (count($infos) == 0) {
            $errMsg = "Aucun compte pour l'adresse $email";
            include(VIEWS."v_oubliMDP.php");
            include(VIEWS."Template/footer.php");
            return;
        }

        include(VIEWS."v_connexion.php");
        include(VIEWS."Template/footer.php");

        //Génération d'un mot de passe de 8 caractères
        $passwd = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 8)), 0, 8);

        $m_user->updateMdp($passwd, $email);

        $header = "MIME-Version: 1.0\r\n";
        $header .= 'From:"Collamaps"<no-reply@collamaps.fr>'."\n";
        $header .= 'Content-Type:text/html; charset="utf-8"'."\n";
        $header .= 'Content-Transfer-Encoding: 8bit';

        $message = '
            <html>
                <body>
                    <div>
                        Bonjour,<br/>
                        Voici votre nouveau mot de passe : '.$passwd.'<br/>
                        Vous pourrez le modifier, après vous être connecté, depuis votre profil.
                        <br/>
                        <br/>
                        Cordialement,<br/>
                        L\'équipe de CollaMaps
                    </div>
                </body>
            </html>
        ';
        try {
            mail($email, "CollaMaps - Récupération de mot de passe", $message, $header);
            ?>
            <script type="text/javascript">
                alert("Un mail de réinitialisation vous a été envoyé");
            </script>
            <?php
        } catch (Exception $e) {
            ?>
            <script type="text/javascript">
                alert("Opération impossible, veuillez rééssayer ultérieurement.");
            </script>
            <?php
        }
    }

    public function validInscription($email, $key) {
        if (!empty($_SESSION['pseudo'])) {
            $this->index();
            return;
        }
        include(MODELS."m_users.php");
        $m_user = new m_Users();

        $retour = $m_user->validInscription($email, $key);

        include(VIEWS.'v_finInscription.php');
        include(VIEWS."Template/footer.php");
    }

    function oubliMDP() {
        if (!empty($_SESSION['pseudo'])) {
            $this->index();
            return;
        }
        include(VIEWS."v_oubliMDP.php");
        include(VIEWS."Template/footer.php");
    }

}
?>
