DROP TABLE IF EXISTS user_list;
DROP TABLE IF EXISTS bannis;
DROP TABLE IF EXISTS carte;
DROP TABLE IF EXISTS utilisateur;

CREATE TABLE utilisateur(
  email VARCHAR(50) PRIMARY KEY,
  pseudo VARCHAR(50) NOT NULL, 
  mdp VARCHAR(100) NOT NULL,
  confirmKey VARCHAR(12),
  valide BOOLEAN, 
  nom VARCHAR(50), 
  prenom VARCHAR(50),
  id_droit VARCHAR(50) CHECK (id_droit IN ('Client','Super-Admin','Admin')),
  date_inscription DATE,
  UNIQUE(pseudo)
)ENGINE=INNODB;

CREATE TABLE carte(
  nom_carte VARCHAR(20) PRIMARY KEY NOT NULL,
  type_carte VARCHAR(20) CHECK (type_carte IN ('Privee','Partagee','Publique')),
  string_carte TEXT,
  busy INTEGER,
  UNIQUE(nom_carte)
)ENGINE=INNODB;

CREATE TABLE bannis(
  id_ban INTEGER PRIMARY KEY AUTO_INCREMENT,
  email_banni VARCHAR(50),
  date_debut DATE NOT NULL,
  date_fin DATE,
  raison TEXT,
  CONSTRAINT fk_email_ban
    FOREIGN KEY (email_banni) 
    REFERENCES utilisateur(email),
  CHECK (date_debut <= date_fin)
)ENGINE=INNODB;


CREATE TABLE user_list(
  nom_carte VARCHAR(20),
  email_utilisateur VARCHAR(50),
  role VARCHAR(14) CHECK (role IN ('Administrateur', 'Consultant', 'Editeur')),
  CONSTRAINT pk_user_list
    PRIMARY KEY (nom_carte, email_utilisateur),
  CONSTRAINT fk_nom_carte
    FOREIGN KEY (nom_carte)
    REFERENCES carte(nom_carte),
  CONSTRAINT fk_email_utilisateur
    FOREIGN KEY (email_utilisateur)
    REFERENCES utilisateur(email)
)ENGINE=INNODB;

INSERT INTO utilisateur VALUES (
	"super-admin@collamaps.fr",
	"super-admin",
	"4a7d1ed414474e4033ac29ccb8653d9b", -- mot de passe non crypté : 0000
	"198567451685",
	1,
	"Saindoux",
	"Gustave",
	"Super-Admin",
	"2011-02-21"
);

INSERT INTO utilisateur VALUES (
	"admin@collamaps.fr",
	"admin",
	"4a7d1ed414474e4033ac29ccb8653d9b", -- mot de passe non crypté : 0000
	"365784596542",
	1,
	"Beland",
	"Dominique",
	"Admin",
	"2011-02-21"
);

INSERT INTO utilisateur VALUES (
	"client@collamaps.fr",
	"client",
	"4a7d1ed414474e4033ac29ccb8653d9b", -- mot de passe non crypté : 0000
	"478775469523",
	1,
	"Bruno",
	"Gamelin",
	"Client",
	"2011-02-21"
);